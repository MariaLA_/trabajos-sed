/* USER CODE BEGIN Header */
/*

 El  siguiente trabajo trata de un paso a nivel. Consta de dos semaforos, uno para los coches y otro para los trenes. Tiene dos modos, automatico y manual. El automatico
 consta de dos sensores HC-SR04 los cuales detectan la presencia de trenes o de coches y en funcion de eso cambian los leds del semaforo correspondiente. Tambien consta de
 una pantalla LCD con comunicacion i2c por el que saldran tres mensajes: "go", "warning" y "stop" correspondientes a los tres estados en los que se pueden encontrar los
 semaforos. Tambien se ha añadido la correspondiente barrera en forma de servo sg90 la cual se mantiene bajada por defecto escepto cuando detecta un coche y no detecta
 trenes.

 También se ha implementado un estado de emergencia el cual se realiza mediante una interrupcion pulsando un boton, al pulsarlo se iluminan todos los leds y se inicia un
 buzzer con el fin de alertar tanto a coches como a trenes. Pulsando otro boton se sale de la interrupcion y se encenderan brevemente ambos leds rojos.

 El segundo estado, es el manual, al cual se pasa mediante el mismo boton que se usa para salir de la interrupcion. Se han añadido dos botones con los cuales se puede simular
 la presencia de un tren y la presencia de un coche. Para solucionar el problema de que la barrera ya no se mueve de forma automatica (de forma intencionada) se ha implementado
 un potenciometro mediante un convertidor analogico digital. Con este potenciometro podremos controlar la barrera de forma manual.

  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dwt_stm32_delay.h"
#include "i2c-lcd.h"
#define SLAVE_ADDRESS_LCD 0x27

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1; //Convertidor ADC
I2C_HandleTypeDef hi2c1; //Comunicacipn I2C
TIM_HandleTypeDef htim1; //Servo
TIM_HandleTypeDef htim3; //Buzzer

/* USER CODE BEGIN PV */

uint32_t sensor_time_c, sensor_time_t, value_p;
uint16_t distance_c, distance_t;
int flag = 1; //Bandera para poder pasar de estado automatico o manual y viceversa

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM3_Init(void);
static void MX_I2C1_Init(void);

/* USER CODE BEGIN PFP */
//Las funciones estan explicadas en la zona de su desarrollo.

void Ultrasonidos(void);
void inicio(void);
void modo_Auto(void);
void modo_Manual(void);
void Botones(void);
int potenciometro(void);
void moverServo(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//Funciones para obtener el tiempo del sensor de ultrasonidos para posteriormente obtener la distancia en centimetros.

uint32_t hcsr04_read (void)
{
 uint32_t local_time=0;

 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);  //trig high
 DWT_Delay_us(10);
 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);  //trig low

 while (!HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_13));  //echo high
 while (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_13))
  {
  local_time++;   // medimos tiempo mientras el pin esta high
  DWT_Delay_us(1);
  };
  return local_time*2;
  }

uint32_t hcsr04_read_2 (void)
{
 uint32_t local_time_2=0;

 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);  //trig high
 DWT_Delay_us(10);  // wait for 10 us
 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);  //trig low


 while (!HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_9));  //echo high
 while (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_9))
  {
  local_time_2++;   //medimos tiempo mientras el pin esta high
  DWT_Delay_us(1);
  };
 return local_time_2*2;
}



//La interrupcion se usará para casos de emergencia. Se encienden todos los leds tanto de los trenes como de los coches.
//Para salir de ese estado, se pulsara el bot_man y se pondrá el semaforo de los coches en rojo y el de los trenes en ambar.

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin==GPIO_PIN_0) //Cuando se pulsa el boton de la interrupcion
	{

			htim3.Instance->CCR1 = 200; //BUZZER

			// Se encienden los leds del tren
			HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOD,ST_AMBAR_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_SET);

			//Se encienden los leds de los coches
			HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOE,SC_AMBAR_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_SET);

			//Como es un estado de emergencia, llamo al buzzer
			//	Buzzer();
			//__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 50);


			//Mientras no pulsemos bot_man, se permanece dentro de la interrupcion
			while(!HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1));

			htim3.Instance->CCR1 = 0; //Desactivo el buzzer

			// Si se pulsa bot_man se pondrán ambos leds rojos

			HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOD,ST_AMBAR_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_RESET);


			HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOE,SC_AMBAR_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_RESET);


			modo_Manual();

	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_I2C1_Init();

  /* USER CODE BEGIN 2 */

  DWT_Delay_Init();
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  inicio();


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 83;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 19999;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 1291;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 254;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, SC_AMBAR_Pin|SC_VERDE_Pin|SC_ROJO_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, TRIGGER_2_Pin|TRIGGER_Pin|ST_ROJO_Pin|ST_AMBAR_Pin
                          |ST_VERDE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : SC_AMBAR_Pin SC_VERDE_Pin SC_ROJO_Pin */
  GPIO_InitStruct.Pin = SC_AMBAR_Pin|SC_VERDE_Pin|SC_ROJO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : BOT_MAN_Pin BOTON_T_Pin BOTON_C_Pin */
  GPIO_InitStruct.Pin = BOT_MAN_Pin|BOTON_T_Pin|BOTON_C_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : INTERRUP_Pin */
  GPIO_InitStruct.Pin = INTERRUP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(INTERRUP_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ECHO_2_Pin ECHO_Pin */
  GPIO_InitStruct.Pin = ECHO_2_Pin|ECHO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : TRIGGER_2_Pin TRIGGER_Pin ST_ROJO_Pin ST_AMBAR_Pin
                           ST_VERDE_Pin */
  GPIO_InitStruct.Pin = TRIGGER_2_Pin|TRIGGER_Pin|ST_ROJO_Pin|ST_AMBAR_Pin
                          |ST_VERDE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

//Funcion principal, por defecto nos encontramos en el modo automatico si pulsamos el boton indicado, se pasa a modo manual

void inicio(void)
{

	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1) == 1 && flag==1)
	{
		flag = 0;
	}

	else if (HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1) == 1 && flag == 0)
	{
		flag = 1;
	}

	switch(flag)
	{
	case 0: modo_Manual(); break;
	case 1: modo_Auto(); break;
	default: modo_Auto(); break;
	}

}

//En esta funcion se recibe el valor del tiempo de cada sensor y se obtienen los valores en centimetros en las variables distance_c y distance_t
void Ultrasonidos(void)
{
		sensor_time_c = hcsr04_read();
		distance_c  = sensor_time_c * 0.034/2;

		sensor_time_t = hcsr04_read_2();
		distance_t = sensor_time_t * 0.034/2;
}

//Los sensores iran dando los valores para determinar si hay un coche o si hay un tren. En consecuencia la barrera se levantara o permanecera bajada y por el LCD saldra
//un mensaje indicando cada estado y lo que el coche puede hacer

void modo_Auto()
{

	Ultrasonidos();

	if (distance_t <= 10 ) //Sensor detecta la presencia del tren por lo que se pone el semaforo en verde para el tren y en rojo para los coches
	{
		TIM1->CCR1=1000; //Barrera bajada

		lcd_init();
		lcd_send_string("      STOP");

		HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOD,ST_AMBAR_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_SET);

		HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOE,SC_AMBAR_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_RESET);

		DWT_Delay_us(4000000);

	}
	else if (distance_t > 10 && distance_c <= 10) //Detecta un coche y no hay ningun tren
		{
			//La barrera se levanta para dejarle pasar
			TIM1->CCR1=3000;

			lcd_init();
			lcd_send_string("       GO");

			HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOD,ST_AMBAR_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_RESET);

			HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,SC_AMBAR_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_SET);

			DWT_Delay_us(4000000);


		}

	else if (distance_t > 10 && distance_c > 10) //No hay ni tren ni coche
	{
		//La barrera tambien esta bajada por si llegase un tren y un coche a la vez, darle prioridad al tren
		TIM1->CCR1=1000;

		lcd_init();
		lcd_send_string("    WARNING!");

		HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_RESET);
		HAL_GPIO_TogglePin(GPIOD,ST_AMBAR_Pin);


		HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_RESET);
		HAL_GPIO_TogglePin(GPIOE,SC_AMBAR_Pin);

		DWT_Delay_us(750000);

	}
}

//Este modo solo funcionara con botones y el potenciometro. Se desactivaran los sensores y en su lugar se usaran dos botones que simularan las distancias predeterminadas.

void modo_Manual(void)
{
	Botones();
	moverServo();
	lcd_clear();
	if (distance_t <= 10 ) //Sensor detecta la presencia del tren por lo que se pone el semaforo en verde para el tren y en rojo para los coches
		{

			HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD,ST_AMBAR_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_SET);

			HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOE,SC_AMBAR_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_RESET);

			DWT_Delay_us(4000000);

		}
		else if (distance_t > 10 && distance_c <= 10) //Detecta un coche y no hay ningun tren
			{

				HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOD,ST_AMBAR_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_RESET);

				HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOE,SC_AMBAR_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_SET);

				DWT_Delay_us(4000000);

			}

		else if (distance_t > 10 && distance_c > 10) //No hay ni tren ni coche
		{

			HAL_GPIO_WritePin(GPIOD,ST_ROJO_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOD,ST_VERDE_Pin,GPIO_PIN_RESET);
			HAL_GPIO_TogglePin(GPIOD,ST_AMBAR_Pin);


			HAL_GPIO_WritePin(GPIOE,SC_ROJO_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOE,SC_VERDE_Pin,GPIO_PIN_RESET);
			HAL_GPIO_TogglePin(GPIOE,SC_AMBAR_Pin);

			DWT_Delay_us(750000);

		}

}



//En esta funcion se sustituyen los sensores por dos botones

void Botones(void)
{

	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_7) && !HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_9))
	{
		distance_t = 5;
	}
	else if (HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_9) && !HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_7))
	{
		distance_c = 5;
	}
	else if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_7) && HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_9))
	{
		distance_t = 5;
		distance_c = 5;
	}
	else
	{
		distance_t = 15;
		distance_c = 15;
	}

}

//Se convierte el valor analogico del potenciometro en digital
int potenciometro(void)
{
	HAL_ADC_Start(&hadc1);
		if(HAL_ADC_PollForConversion(&hadc1,HAL_MAX_DELAY) == HAL_OK)
	value_p = (HAL_ADC_GetValue(&hadc1)*12+1);
	HAL_ADC_Stop(&hadc1);
	return value_p;
}

//Se coge el valor del potenciometro y en consecuencia se mueve el servo
void moverServo(void)
{
	uint32_t value;
	value = potenciometro();
	if (value < 1000) value = 1000;
	else if (value>3000) value = 3000;
	TIM1->CCR1 = value;

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
