/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SC_AMBAR_Pin GPIO_PIN_3
#define SC_AMBAR_GPIO_Port GPIOE
#define SC_VERDE_Pin GPIO_PIN_5
#define SC_VERDE_GPIO_Port GPIOE
#define BOT_MAN_Pin GPIO_PIN_1
#define BOT_MAN_GPIO_Port GPIOC
#define INTERRUP_Pin GPIO_PIN_0
#define INTERRUP_GPIO_Port GPIOA
#define INTERRUP_EXTI_IRQn EXTI0_IRQn
#define CONV_ADC_Pin GPIO_PIN_2
#define CONV_ADC_GPIO_Port GPIOA
#define BUZZER_Pin GPIO_PIN_6
#define BUZZER_GPIO_Port GPIOA
#define SERVO_Pin GPIO_PIN_9
#define SERVO_GPIO_Port GPIOE
#define ECHO_2_Pin GPIO_PIN_9
#define ECHO_2_GPIO_Port GPIOD
#define TRIGGER_2_Pin GPIO_PIN_11
#define TRIGGER_2_GPIO_Port GPIOD
#define ECHO_Pin GPIO_PIN_13
#define ECHO_GPIO_Port GPIOD
#define TRIGGER_Pin GPIO_PIN_15
#define TRIGGER_GPIO_Port GPIOD
#define BOTON_T_Pin GPIO_PIN_7
#define BOTON_T_GPIO_Port GPIOC
#define BOTON_C_Pin GPIO_PIN_9
#define BOTON_C_GPIO_Port GPIOC
#define ST_ROJO_Pin GPIO_PIN_1
#define ST_ROJO_GPIO_Port GPIOD
#define ST_AMBAR_Pin GPIO_PIN_3
#define ST_AMBAR_GPIO_Port GPIOD
#define ST_VERDE_Pin GPIO_PIN_5
#define ST_VERDE_GPIO_Port GPIOD
#define SC_ROJO_Pin GPIO_PIN_1
#define SC_ROJO_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
