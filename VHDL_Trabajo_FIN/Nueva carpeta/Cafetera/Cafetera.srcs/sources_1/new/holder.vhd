library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity holder_e is
  Port (
    clk_in: IN std_logic;
    bot_in: IN std_logic;
    estado_out: IN std_logic_vector(3 downto 0);
    bot_out: OUT std_logic
  );
end holder_e;

architecture Behavioral of holder_e is
    signal but_hold: std_logic;
begin
    process(bot_in, clk_in)
    begin
        
        if rising_edge(clk_in) then
            if (bot_in = '1') then
                but_hold <= '1';
            elsif ((but_hold = '1') and (estado_out = "0010")) then --Seleccion bebida
                but_hold <= '0';      
            end if;
        end if;
 
    end process;

    bot_out <= but_hold;
    
end Behavioral;