library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY Debounce IS
GENERIC ( N: natural := 8 );        --LONGITUD DEL registro de desplazamiento
port (
clk : in std_logic;
rst : in std_logic;
btn_in : in std_logic;
btn_out : out std_logic);
END Debounce;


ARCHITECTURE behavioral OF Debounce IS

signal Q : unsigned (N DOWNTO 1);  --Los 8 bits de los biestables del Antirebotes
signal butn_out : std_logic;
BEGIN
process(clk)
begin
    if ( rising_edge(clk) ) then

        if (rst = '1') then     --Reset todo a 0
            Q <= to_unsigned(0 , Q'length );
            butn_out <='0';
                      
        elsif (butn_out ='1' and btn_in='1') then 
                     butn_out <='1';      --Mantiene la salida en caso de que se detecte el flanco positivo 
                                           --con recursividad a la misma funci�n
        else    
            Q(1) <= btn_in;  --Registro de desplazamiento de los biestables
           for i  IN 2 to N LOOP
            Q(i) <= Q(i-1);
            end loop;
         end if; end if; 
     
      for i IN 2 to N-1 LOOP        --La salida se actualiza constantemente
         butn_out <= Q(i) and Q(i-1);
            end loop;  
               
     
end process;

btn_out <=    butn_out and not(Q(N));   --La salida ser� los dos primeros biestables y 
                                    --que el 3� marque el FP. Mostrar� los FP del btn_in
END behavioral;