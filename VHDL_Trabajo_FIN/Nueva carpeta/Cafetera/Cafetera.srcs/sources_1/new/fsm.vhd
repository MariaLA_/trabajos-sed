library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fsm is
    Port ( 
    start: in std_logic:='0'; -- Switch para encender la cefetera
    q_cafe: in std_logic:='0'; -- Boton empezar servir cafe
    reset: in std_logic:='0'; -- Boton de reset
    clk_out: in std_logic; -- Reloj
    temp1 : in std_logic; -- Temporizador Bebidas
    temp2 : in std_logic; -- Temporizador Leche
    temp3 : in std_logic; -- Temporizador Azucar
    boton_seleccion: in std_logic:='0'; -- Boton para seleccionar el tipo de bebida o la cantidad de leche o azucar
    cambio: in std_logic:='0'; -- Boton mas para leche o azucar, coincide con el boton cambio de las bebidas
    estado_cafe: in std_logic_vector (1 downto 0); -- Salida que se observa en las bebidas sin haber seleccionado una
    estado_azucar: in std_logic_vector (1 downto 0);-- Salida que se observa en la leche sin haber seleccionado un nivel
    estado_leche: in std_logic_vector (1 downto 0);-- Salida que se observa en el azucar sin haber seleccionado un nivel
    salida_cafe: out std_logic_vector (1 downto 0);
    salida_leche: out std_logic_vector (1 downto 0);
    salida_azucar: out std_logic_vector (1 downto 0);
    motor_cafe: out std_logic; --led motor cafe
    motor_azucar: out std_logic; -- led motor azucar
    motor_leche: out std_logic; -- led motor leche
    led_seleccion: out std_logic; --led seleccionando bebida, leche o azucar
    led_standby: out std_logic; --led que indica que la cafetera esta en stanby
    led_apagado : out std_logic;  -- led que indica que la cafetera esta apagada
    led_encendido : out std_logic; --led que indica que la cafetera esta encendida
    d1: out std_logic_vector (4 downto 0); -- Primer display
    d2: out std_logic_vector (4 downto 0); -- Segundo display
    d3: out std_logic_vector (4 downto 0); -- Tercer display
    d4: out std_logic_vector (4 downto 0); -- Cuarto display
    d5: out std_logic_vector (4 downto 0); -- Quinto display
    d6: out std_logic_vector (4 downto 0); -- Sexto display
    d7: out std_logic_vector (4 downto 0); -- Septimo display
    d8: out std_logic_vector (4 downto 0); -- Octavo display
    estado_actual_salida: out std_logic_vector(3 downto 0):="0000"
    );
end fsm;

architecture Behavioral of fsm is
    signal estado : std_logic_vector(3 downto 0);
    signal aux_estado : std_logic_vector(3 downto 0):="0000";
    signal estado_actual: integer range 0 to 7; 
begin
    
    process(start, q_cafe, boton_seleccion, cambio, clk_out, reset ,estado, temp2, temp3, temp1, aux_estado, estado_cafe, estado_leche, estado_azucar) 
  
    begin
    
   if (rising_edge (clk_out)) then   
   
        case (estado) is
    
            when "0000" => -- Apagado
                if (start = '1') then
                     aux_estado <= "0001";
                end if;
            
            when "0001" => -- Standby
                if (q_cafe = '1') then
                    aux_estado <= "0010";
                elsif (start = '0') then
                    aux_estado <= "0000";
                end if;
                
            when "0010" => -- Seleccion del tipo de bebida
                if (boton_seleccion = '1' and estado_cafe = "00") then
                    salida_cafe <= "00";
                    aux_estado <= "0011";
                elsif (boton_seleccion = '1' and estado_cafe = "01") then
                     salida_cafe <= "01";
                     aux_estado <= "0011"; 
                elsif (boton_seleccion = '1' and estado_cafe = "10") then
                     salida_cafe <= "10";
                     aux_estado <= "0011";
                  elsif (boton_seleccion = '0') then
                      salida_cafe <= "11";   
                      aux_estado <= "0010";   
                end if;
            
            when "0011" => -- La bebida se esta echando
                if (temp1 = '0' ) then 
                    aux_estado <= "0100";
                elsif (temp1 = '1' ) then    
                    aux_estado <= "0011";
                end if;
            
            when "0100" => -- Seleccion del nivel de leche
                if (boton_seleccion = '1' and estado_leche = "00") then
                    salida_leche <= "00";
                    aux_estado <= "0110";
                elsif (boton_seleccion = '1' and estado_leche = "01") then
                     salida_leche <= "01";
                     aux_estado <= "0101"; 
                elsif (boton_seleccion = '1' and estado_leche = "10") then
                     salida_leche <= "10";
                     aux_estado <= "0101";
                elsif (boton_seleccion = '0') then
                      salida_leche <= "11"; 
                      aux_estado <= "0100";       
                end if;
            
            when "0101" => -- La leche se esta echando
                if (temp2 = '0') then 
                    aux_estado <= "0110";
                elsif (temp2 = '1') then    
                    aux_estado <= "0101";      
                end if;
            
            when "0110" => -- Seleccion del nivel de azucar
                
                if (boton_seleccion = '1' and estado_azucar = "00") then
                    salida_azucar <= "00";
                    salida_leche<="11";
                    aux_estado <= "0001";
                elsif (boton_seleccion = '1' and estado_azucar = "01") then
                     salida_azucar <= "01";
                     aux_estado <= "0111"; 
                elsif (boton_seleccion = '1' and estado_azucar = "10") then
                     salida_azucar <= "10";
                     salida_leche<="11";
                     aux_estado <= "0111";
                 elsif (boton_seleccion = '0') then
                      salida_azucar <= "11"; 
                      salida_leche<="11";
                      aux_estado <= "0110";    
                end if;
                
             when "0111" => -- El azucar se esta echando
                if (temp3 = '0') then 
                    aux_estado <= "0001";
                elsif (temp3 = '1') then    
                    aux_estado <= "0111";         
                end if;
              
                
            when others => aux_estado <= "0000";
                
                
        end case;
        
       end if;
        
    estado <= aux_estado;        
     
     case estado is
     
            when "0000" => 
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '0';
                led_apagado <= '1'; 
                led_encendido <= '0'; 
                led_standby <= '0'; 
                d1 <= "01000";--A
                d2 <= "10000";--P
                d3 <= "01000";--A
                d4 <= "01001";--G
                d5 <= "01000";--A
                d6 <= "01101";--D 
                d7 <= "01000";--A
                d8 <= "11111";
                estado_actual_salida <= "0000";
                
            when "0001" =>
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '0';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '1';
                d1 <= "10000";--P
                d2 <= "01000";--A
                d3 <= "00101";--R
                d4 <= "01000";--A
                d5 <= "01101";--D
                d6 <= "01000";--A
                d7 <= "11111";
                d8 <= "11111";
                estado_actual_salida <= "0001";
                
            when "0010" =>
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '1';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '0';
                d6 <= "11111"; 
                d7 <= "11111"; 
                d8 <= "11111";
                if(estado_cafe = "00")then
                    d1<="00011"; --C
                    d2<="00100"; --O
                    d3<="00101"; --R
                    d4<="00110"; --T
                    d5<="00100"; --O
                elsif(estado_cafe = "01")then
                    d1<="00111"; --L
                    d2<="01000"; --A
                    d3<="00101"; --R
                    d4<="01001"; --G
                    d5<="00100"; --O
                elsif(estado_cafe = "10")then
                    d1<="00011"; --C
                    d2<="01010"; --H
                    d3<="00100"; --O
                    d4<="00011"; --C
                    d5<="00100"; --O
                end if;    
                estado_actual_salida <= "0010";
                
            when "0011" =>
                motor_cafe <= '1';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '0';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '0';
                d1 <= "01011";--E
                d2 <= "00011";--C
                d3 <= "01010";--H
                d4 <= "01000";--A
                d5 <= "01100";--N
                d6 <= "01101";--D 
                d7 <= "00100";--O
                d8 <= "11111";
                estado_actual_salida <= "0011";  
                 
            when "0100" =>
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '1';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '0';
                d1 <= "00111";--L
                d2 <= "01011";--E
                d3 <= "00011";--C
                d4 <= "01010";--H
                d5 <= "01011";--E
                d6 <= "10001";--- 
                d8 <= "11111";
                if(estado_leche = "00")then
                    d7<="00000"; --0
                elsif(estado_leche = "01")then
                    d7<="00001"; --1
                elsif(estado_leche = "10")then
                    d7<="00010"; --2
                end if;    
                estado_actual_salida <= "0100";
                
            when "0101" =>
                motor_cafe <= '0';
                motor_leche <= '1';
                motor_azucar <= '0';
                led_seleccion <= '0';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '0';
                d1 <= "01011";--E
                d2 <= "00011";--C
                d3 <= "01010";--H
                d4 <= "01000";--A
                d5 <= "01100";--N
                d6 <= "01101";--D 
                d7 <= "00100";--O
                d8 <= "11111";
                estado_actual_salida <= "0101";
                
            when "0110" =>
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '1';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '0';
                d1 <= "01000";--A
                d2 <= "01110";--Z
                d3 <= "01111";--U
                d4 <= "00011";--C
                d5 <= "01000";--A
                d6 <= "00101";--R 
                d7 <= "10001";---
                if(estado_leche = "00")then
                    d8<="00000"; --0
                elsif(estado_leche = "01")then
                    d8<="00001"; --1
                elsif(estado_leche = "10")then
                    d8<="00010"; --2
                end if;    
                estado_actual_salida <= "0110";
                
            when "0111" =>
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '1';
                led_seleccion <= '0';
                led_apagado <= '0'; 
                led_encendido <= '1'; 
                led_standby <= '0';
                d1 <= "01011";--E
                d2 <= "00011";--C
                d3 <= "01010";--H
                d4 <= "01000";--A
                d5 <= "01100";--N
                d6 <= "01101";--D 
                d7 <= "00100";--O
                d8 <= "11111";
                estado_actual_salida <= "0111";
                
            when others =>
                motor_cafe <= '0';
                motor_leche <= '0';
                motor_azucar <= '0';
                led_seleccion <= '0';
                led_apagado <= '1'; 
                led_encendido <= '0';
                led_standby <= '0'; 
                d1 <= "11111";
                d2 <= "11111";
                d3 <= "11111";
                d4 <= "11111";
                d5 <= "11111";
                d6 <= "11111"; 
                d7 <= "11111";
                d8 <= "11111";
                estado_actual_salida <= "0000";
                
            end case; 
     
        
     end process;
  
     
                 
end Behavioral;