LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;


ENTITY Decoder IS
      PORT (
                code : IN std_logic_vector(4 DOWNTO 0);
                led : OUT std_logic_vector(6 DOWNTO 0)
            );
END ENTITY Decoder;

ARCHITECTURE dataflow OF decoder IS

BEGIN

            WITH code SELECT
                   led <= "0000001" WHEN "00000",--0
                            "1001111" WHEN "00001",--1
                          "0010010" WHEN "00010",--2
                          "0110001" WHEN "00011",--C 
                          "1100010" WHEN "00100",--o 
                          "1111010" WHEN "00101",--r 
                          "1110000" WHEN "00110",--t 
                          "1110001" WHEN "00111",--L 
                          "0001000" WHEN "01000",--A 
                          "0100001" WHEN "01001",--G 
                          "1001000" WHEN "01010",--H 
                          "0110000" WHEN "01011",--E
                          "0001001" WHEN "01100",--n 
                          "1000010" WHEN "01101",--d 
                          "0010010" WHEN "01110",--Z 
                          "1000001" WHEN "01111",--U
                          "0011000" WHEN "10000",--P
                          "1111110" WHEN "10001",---   
                          "1111111" WHEN others;
                          
END ARCHITECTURE dataflow;