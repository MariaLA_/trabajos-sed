library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Temporizador_1 is

 port(
 tipo : in std_logic_vector (1 downto 0):="11";
 clk : in std_logic;
 reset : in std_logic;
 ticks : inout integer:=0;
 seg : inout integer:=0;
 activo : out std_logic:='0'
 );
 
end Temporizador_1;

architecture Behavioral of Temporizador_1 is



begin
    process (clk, tipo)
   
   variable segu, tic : integer:=0;
   
     begin
     
     
      case tipo is 
           
            when "00" => tic :=30;
            when "01" => tic :=40;   
            when "10" => tic :=50;
            when others => tic :=0;
            
            end case;
     
        if (rising_edge (clk)) then
             
            
               
            if (tic /=0) then
                segu := segu+1;
                if (segu = tic) then
                    activo <= '0';
                    segu := 0;
                    tic := 0;
                elsif(segu /= tic) then
                    activo <= '1';
                end if;
            end if;
            
            if (tic =0) then 
                activo <= '0';
                segu := 0;
            end if;    
            
        end if;    
         seg <= segu;
         ticks <=  tic;   
   end process;
      
end Behavioral;