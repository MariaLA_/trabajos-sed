library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Bebidas is
    Port (
    seleccion : in std_logic:='0'; --Boton para seleccionar la bebida
    cambio : in std_logic:='0'; --Boton para cambiar el tipo de bebida
    reset : in std_logic:='0';
    clk : in std_logic:='0';
    estado_aux : inout std_logic_vector(1 downto 0):="00";
    --El boton de seleccion tiene prevalencia al de cambio
    estado: out std_logic_vector (1 downto 0);
    estado_next: out std_logic_vector (1 downto 0)
  );  
end Bebidas;

architecture Behavioral of Bebidas is

signal seleccion_bebida: std_logic_vector (1 downto 0):= "00";
signal aux_seleccion : std_logic_vector (1 downto 0):= "00";

begin

 process (seleccion,cambio,clk,seleccion_bebida,estado_aux)
   begin
   
     if (reset = '0' and rising_edge (clk)) then
  
        case estado_aux is
        
                when "00"=>
                   estado <= "00";
                   if cambio= '1' then 
                    estado_aux <= "01";
                    estado_next <= "01";
                    else
                    estado_aux <= "00";
                    end if;
                    
                when "01"=>
                    estado <= "01";
                    if cambio= '1' then 
                    estado_aux <= "10";
                    estado_next <= "10";
                    else
                    estado_aux <= "01";
                    end if;
                    
                when "10"=>
                    estado <= "10";
                    if cambio= '1' then 
                    estado_aux <= "00";
                    estado_next <= "00";
                    else
                    estado_aux <= "10";
                    end if;
                    
                when others => estado_aux <= "11";
                
        end case;
  
        
    end if;  
  
         
end process;

end Behavioral;
