library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity holder_mmc is
  Port (
    clk_in: IN std_logic;
    bot_in: IN std_logic;
    estado_out: IN std_logic_vector(1 downto 0);
    estado_next_out : IN std_logic_vector(1 downto 0);
    bot_out: OUT std_logic
  );
end holder_mmc;

architecture Behavioral of holder_mmc is
    signal but_hold: std_logic;
begin
    process(bot_in, clk_in)
    begin
        
        if rising_edge(clk_in) then
            if (bot_in = '1') then
                but_hold <= '1';
            elsif ((but_hold = '1') and (estado_out = "10")) then
                if (estado_next_out = "00") then but_hold <= '0';
                elsif (estado_out = "01") then but_hold <= '0';
                end if;
            elsif ((but_hold = '1') and (estado_out = "00")) then
                if (estado_next_out = "01") then but_hold <= '0';
                elsif (estado_next_out = "10") then but_hold <= '0';
                end if;
            elsif ((but_hold = '1') and (estado_out = "01")) then
                if (estado_next_out = "00") then but_hold <= '0';
                elsif (estado_next_out = "10") then but_hold <= '0';
                end if;      
            end if;
        end if;
 
    end process;

    bot_out <= but_hold;
    
end Behavioral;