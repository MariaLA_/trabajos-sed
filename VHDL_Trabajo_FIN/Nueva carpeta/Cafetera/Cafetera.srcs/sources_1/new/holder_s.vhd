library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity holder_s is
  Port (
    clk_in:   IN std_logic;
    bot_in:   IN std_logic;
    estado_out: IN std_logic_vector(3 downto 0);
    t1_out : in std_logic;
    t2_out : in std_logic;
    t3_out : in std_logic;
    salida_leche :in std_logic_vector (1 downto 0);
    salida_azucar : in std_logic_vector (1 downto 0);
    bot_out:   OUT std_logic
  );
end holder_s;

architecture Behavioral of holder_s is
    signal but_hold: std_logic;
begin
    process(bot_in, clk_in, t1_out, t2_out, t3_out)
    begin
        
        if rising_edge(clk_in) then
            if (bot_in = '1') then
                but_hold <= '1';
            elsif ((but_hold = '1') and (estado_out = "0011")) then --Echando bebida
                if (t1_out = '1') then but_hold <= '1';
                elsif (t1_out = '0') then but_hold <= '0';
                end if;
            elsif ((but_hold = '1') and (estado_out = "0101")) then --Echando leche
                if (t2_out = '1' and salida_leche /= "00") then but_hold <= '1';
                elsif (t2_out = '0' and salida_leche /= "00") then but_hold <= '0';
                end if;   
            elsif ((but_hold = '1') and (estado_out = "0111")) then --Echando azucar
                if (t3_out = '1' and salida_azucar/="00") then but_hold <= '1';
                elsif (t3_out = '0' and salida_azucar/="00") then but_hold <= '0';
                end if;
            elsif ((but_hold = '1') and (estado_out = "0110")) then
                  if (salida_leche = "00") then but_hold <= '0';
                  end if;   
             elsif ((but_hold = '1') and (estado_out = "0001")) then
                  if (salida_azucar = "00") then but_hold <= '0';
                  end if;        
                  
            end if;
        end if;
 
    end process;

    bot_out <= but_hold;
    
end Behavioral;