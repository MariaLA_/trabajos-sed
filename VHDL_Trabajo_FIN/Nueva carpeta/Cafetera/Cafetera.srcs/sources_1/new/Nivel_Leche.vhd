library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity Nivel_Leche is
    PORT (
    seleccion : in std_logic:='0';
    cambio : in std_logic:='0';
    clk : in std_logic:='0';
    estado_aux : inout std_logic_vector(1 downto 0):="00";
    --El boton de seleccion tiene prevalencia sobre los botones de mas o menos
    estado: out std_logic_vector (1 downto 0);
    estado_next: out std_logic_vector (1 downto 0)
    );
end Nivel_Leche;


architecture Behavioral of Nivel_Leche is

--signal ma,me: integer:= 0;
signal nivel_leche: std_logic_vector (1 downto 0):= "00";
signal aux_seleccion : std_logic_vector (1 downto 0):= "00";

begin

    process (cambio,seleccion,clk,nivel_leche,aux_seleccion)
    begin
     if (rising_edge (clk)) then
        case estado_aux is
                           
        
                when "00"=>
                   estado <= "00";
                   if cambio= '1' then 
                    estado_aux <= "01";
                    estado_next <= "01";
                    else
                    estado_aux <= "00";
                    end if;
                    
                when "01"=>
                    estado <= "01";
                    if cambio= '1' then 
                    estado_aux <= "10";
                    estado_next <= "10";
                    else
                    estado_aux <= "01";
                    end if;
                    
                when "10"=>
                    estado <= "10";
                    if cambio= '1' then 
                    estado_aux <= "00";
                    estado_next <= "00";
                    else
                    estado_aux <= "10";
                    end if;
                    
                when others => estado_aux <= "11";
       end case;
        
     end if;
     
     
  end process;

end architecture;