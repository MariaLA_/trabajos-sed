library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Cafetera is
port(
        CLK : in std_logic;
        START : in std_logic; -- Switch de encendido de la cafetera 
        RESET : in std_logic; -- Boton de reset
        CAMBIO: in std_logic; -- Boton de mas y cambio
        SELECCION: in std_logic; -- Boton de seleccion de las bebidas, azucar o leche
        EMPEZAR: in std_logic; -- Boton para pasar de standby a realizar la bebida
        LED_APAGADO: out std_logic; -- Encendido si la cafetera esta apagada
        LED_ENCENDIDO: out std_logic; -- Encendido si la cafetera esta encendida
        LED_SELECCIONANDO: out std_logic; -- Encendido si se esta eligiendo tipo de bebida o niveles de leche o azucar 
        LED_STANDBY: out std_logic; --Encendido cuando la cafetera esta encendida y esperando para realizar la bebida
        LED_BOMBA_CAFE: out std_logic; -- Encendido cuendo se esta echando la bebida
        LED_BOMBA_LECHE: out std_logic; -- Encendido cuando se esta echando la leche
        LED_BOMBA_AZUCAR: out std_logic; -- Encendido cuando se esta echando el azucar
        display_select : out STD_LOGIC_VECTOR (7 downto 0);
        display_number : out STD_LOGIC_VECTOR (6 downto 0)
);

end Cafetera;

architecture Behavioral of Cafetera is


COMPONENT holder_mmc
 PORT(
        clk_in: IN std_logic;
        bot_in: IN std_logic;
        estado_out: IN std_logic_vector(1 downto 0);
        bot_out: OUT std_logic;
        estado_next_out : IN std_logic_vector(1 downto 0)
     );
END COMPONENT;

COMPONENT holder_e
 PORT(
        clk_in: IN std_logic;
        bot_in: IN std_logic;
        estado_out: IN std_logic_vector(3 downto 0);
        bot_out: OUT std_logic
     );
END COMPONENT;

COMPONENT holder_s
 PORT(
        clk_in:   IN std_logic;
        bot_in:   IN std_logic;
        estado_out: IN std_logic_vector(3 downto 0);
        t1_out : in std_logic;
        t2_out : in std_logic;
        t3_out : in std_logic;
        salida_leche :in std_logic_vector (1 downto 0);
        salida_azucar : in std_logic_vector (1 downto 0);
        bot_out:   OUT std_logic
     );
END COMPONENT;


COMPONENT Debounce
    PORT ( 
           clk : in STD_LOGIC; -- 100 MHz
           btn_in : in std_logic;
           rst : in std_logic;
           btn_out : out std_logic
           );
    END COMPONENT;

COMPONENT clk_divider
	generic ( relacion : integer := 10000000);
    PORT ( 
           clk : in STD_LOGIC; -- 100 MHz
           reset : in STD_LOGIC;
           clk_out : out STD_LOGIC
           );
	END COMPONENT;
	
COMPONENT Decoder
	PORT( 
	      code : in STD_LOGIC_VECTOR (4 downto 0);
	      led : out STD_LOGIC_VECTOR (6 downto 0)
	      );
	END COMPONENT;

COMPONENT synchrnzr
 PORT(
        clk : in std_logic;
        sync_in : in std_logic;
        sync_out : out std_logic
      );
END COMPONENT;

COMPONENT display_refresh
	PORT( 
	      clk : in STD_LOGIC; -- 400 Hz
	      reset : in STD_LOGIC;
	      segment_1 : in STD_LOGIC_VECTOR(6 downto 0);
	      segment_2 : in STD_LOGIC_VECTOR(6 downto 0);
	      segment_3 : in STD_LOGIC_VECTOR(6 downto 0);
          segment_4 : in STD_LOGIC_VECTOR(6 downto 0);
          segment_5 : in STD_LOGIC_VECTOR(6 downto 0);
          segment_6 : in STD_LOGIC_VECTOR(6 downto 0);
          segment_7 : in STD_LOGIC_VECTOR(6 downto 0);
          segment_8 : in STD_LOGIC_VECTOR(6 downto 0);
          display_select : out STD_LOGIC_VECTOR(7 downto 0);
          display_number : out STD_LOGIC_VECTOR(6 downto 0)
          );
	END COMPONENT;	

COMPONENT Temporizador_1
    PORT(
        tipo : in std_logic_vector (1 downto 0);
        clk : in std_logic;
        reset : in std_logic;
        ticks : inout integer;
        seg : inout integer;
        activo : out std_logic
          );
    END COMPONENT;

COMPONENT Temporizador_2
    PORT(
        tipo : in std_logic_vector (1 downto 0);
        clk : in std_logic;
        reset : in std_logic;
        ticks : inout integer;
        seg : inout integer;
        activo : out std_logic
          );
    END COMPONENT;

COMPONENT Bebidas
    PORT(
        seleccion : in std_logic; 
        cambio : in std_logic; 
        reset : in std_logic;
        clk : in std_logic;
        estado_aux : inout std_logic_vector(1 downto 0); 
        estado: out std_logic_vector (1 downto 0);
        estado_next: out std_logic_vector (1 downto 0)
    );
    END COMPONENT;

COMPONENT Nivel_Leche
    PORT(
        seleccion : in std_logic;
        cambio : in std_logic;
        clk : in std_logic;
        estado_aux : inout std_logic_vector(1 downto 0);
        estado: out std_logic_vector (1 downto 0);
        estado_next: out std_logic_vector (1 downto 0)
    );
    END COMPONENT;

COMPONENT Nivel_Azucar
      PORT(
        seleccion : in std_logic;
        cambio : in std_logic;
        clk : in std_logic;
        estado_aux : inout std_logic_vector(1 downto 0);
        estado: out std_logic_vector (1 downto 0)
    );
    END COMPONENT;

COMPONENT fsm
    PORT(
        start: in std_logic; 
        q_cafe: in std_logic;
        reset: in std_logic; 
        clk_out: in std_logic; 
        temp1 : in std_logic;
        temp2 : in std_logic;
        temp3 : in std_logic;
        boton_seleccion: in std_logic; 
        cambio: in std_logic; 
        estado_cafe: in std_logic_vector (1 downto 0); 
        estado_azucar: in std_logic_vector (1 downto 0);
        estado_leche: in std_logic_vector (1 downto 0);
        salida_cafe: out std_logic_vector (1 downto 0);
        salida_leche: out std_logic_vector (1 downto 0);
        salida_azucar: out std_logic_vector (1 downto 0);
        motor_cafe: out std_logic; 
        motor_azucar: out std_logic; 
        motor_leche: out std_logic; 
        led_seleccion: out std_logic;
        led_apagado : out std_logic; 
        led_encendido : out std_logic; 
        led_standby: out std_logic;
        d1: out std_logic_vector (4 downto 0);
        d2: out std_logic_vector (4 downto 0); 
        d3: out std_logic_vector (4 downto 0); 
        d4: out std_logic_vector (4 downto 0); 
        d5: out std_logic_vector (4 downto 0); 
        d6: out std_logic_vector (4 downto 0);
        d7: out std_logic_vector (4 downto 0);
        d8: out std_logic_vector (4 downto 0);
        estado_actual_salida: out std_logic_vector(3 downto 0)
    );
    END COMPONENT;

signal clk_fsm, clk_display : STD_LOGIC;
signal cambio_debounce, seleccion_debounce, empezar_debounce : std_logic;
signal cambio_sync, seleccion_sync, empezar_sync : std_logic;
signal dis1, dis2, dis3, dis4, dis5, dis6, dis7, dis8 : std_logic_vector(4 downto 0);
signal segment1, segment2, segment3, segment4, segment5, segment6, segment7, segment8 : std_logic_vector(6 downto 0);
signal bebida_estado, leche_estado, azucar_estado : std_logic_vector(1 downto 0);
signal bebida_salida, leche_salida, azucar_salida : std_logic_vector(1 downto 0);
signal salida_t1, salida_t2, salida_t3 : std_logic;
signal cambio_holder, seleccion_holder, empezar_holder : std_logic; 
signal salida_fsm : std_logic_vector(3 downto 0);
signal bebida_estado_next, leche_estado_next : std_logic_vector(1 downto 0);

begin

        clk_divider_fsm_1Hz: clk_divider
        GENERIC MAP( relacion => 50000000) -- 1 Hz a fsm
        PORT MAP(

            clk => CLK, -- 100 MHz
            reset => RESET,
            clk_out => clk_fsm -- 1 Hz
            );
            
        clk_divider_displays_400Hz: clk_divider
		GENERIC MAP( relacion => 125000) -- 400 Hz a displays
		PORT MAP(
			clk => CLK, -- 100 MHz
    		reset => RESET,
		    clk_out => clk_display -- 400 Hz
		    );
		
		
		debounce_cambio : Debounce
		PORT MAP(  
		   clk => CLK,
           btn_in => CAMBIO,
           rst => RESET,
           btn_out => cambio_debounce		
		);
		
		
		debounce_seleccion : Debounce
		PORT MAP(  
		   clk => CLK,
           btn_in => SELECCION,
           rst => RESET,
           btn_out => seleccion_debounce		
		);
		
	   debounce_empezar : Debounce
		PORT MAP(  
		   clk => CLK,
           btn_in => EMPEZAR,
           rst => RESET,
           btn_out => empezar_debounce		
		);
		
	   
	   sync_cambio : synchrnzr
		PORT MAP(  
		   clk => CLK,
           sync_in => cambio_debounce,
           sync_out => cambio_sync	
		);
		
	
		sync_seleccion : synchrnzr
		PORT MAP(  
		   clk => CLK,
           sync_in => seleccion_debounce,
           sync_out => seleccion_sync	
		);
	   
	    sync_empezar : synchrnzr
	    PORT MAP(  
		   clk => CLK,
           sync_in => empezar_debounce,
           sync_out => empezar_sync	
		);
	
		
		holder_cambio : holder_mmc
		PORT MAP(
		  clk_in => CLK,
          bot_in => cambio_sync,
          estado_out => bebida_estado,
          bot_out => cambio_holder,
          estado_next_out => bebida_estado_next
		);

		
		
	    holder_seleccion : holder_s
		PORT MAP(
		  clk_in => CLK,
          bot_in => seleccion_sync,
          estado_out => salida_fsm,
          t1_out => salida_t1,
          t2_out => salida_t2,
          t3_out => salida_t3,
          salida_leche => leche_salida,
          salida_azucar => azucar_salida,
          bot_out => seleccion_holder
		);
		
		holder_empezar : holder_e
		PORT MAP(
		  clk_in => CLK,
          bot_in => empezar_sync,
          estado_out => salida_fsm,
          bot_out => empezar_holder
		);
		
		
		bebida : Bebidas
	    PORT MAP(
	       seleccion => seleccion_holder,
           cambio => cambio_holder,
           reset => RESET,
           clk => clk_fsm,
           estado => bebida_estado,
           estado_next => bebida_estado_next
	       );
	   
	      leche : Nivel_Leche
	      PORT MAP(
	       seleccion => seleccion_holder,
           cambio => cambio_holder,
           clk => clk_fsm,
           estado => leche_estado,
           estado_next => leche_estado_next
	      ); 
	      
	      azucar : Nivel_Azucar
	      PORT MAP(
	       seleccion => seleccion_holder,
           cambio => cambio_holder,
           clk => clk_fsm,
           estado => azucar_estado
	       ); 
	      
           temporizador_bebida : Temporizador_1	   
	       PORT MAP(
	        tipo => bebida_salida,
            clk => clk_fsm,
            reset => RESET,
            activo => salida_t1
	       );
	       
	       temporizador_leche : Temporizador_2	   
	       PORT MAP(
	        tipo => leche_salida,
            clk => clk_fsm,
            reset => RESET,
            activo => salida_t2
	       );
	       
	       temporizador_azucar : Temporizador_2	   
	       PORT MAP(
	        tipo => azucar_salida,
            clk => clk_fsm,
            reset => RESET,
            activo => salida_t3
	       );
	       
	       maquina_estados : fsm
	       PORT MAP(
	        start => START,
            q_cafe => empezar_holder,
            reset => RESET,
            clk_out => clk_fsm,
            temp1 => salida_t1,
            temp2 => salida_t2,
            temp3 => salida_t3,
            boton_seleccion => seleccion_holder,
            cambio => cambio_holder, 
            estado_cafe => bebida_estado,
            estado_azucar => azucar_estado,
            estado_leche => leche_estado,
            salida_cafe => bebida_salida,
            salida_leche => leche_salida,
            salida_azucar => azucar_salida,
            motor_cafe => LED_BOMBA_CAFE,
            motor_azucar => LED_BOMBA_AZUCAR,
            motor_leche => LED_BOMBA_LECHE,
            led_seleccion => LED_SELECCIONANDO,
            led_apagado => LED_APAGADO,
            led_encendido => LED_ENCENDIDO,
            led_standby => LED_STANDBY,
            d1 => dis1,
            d2 => dis2,
            d3 => dis3, 
            d4 => dis4,
            d5 => dis5,
            d6 => dis6,
            d7 => dis7,
            d8 => dis8,
            estado_actual_salida => salida_fsm
	       );
	       
	       decoder1: Decoder
           PORT MAP(
            code => dis1,
            led => segment1
            );
	       
	       decoder2: Decoder
           PORT MAP(
            code => dis2,
            led => segment2
            );
            
           decoder3: Decoder
           PORT MAP(
            code => dis3,
            led => segment3
            );
            
           decoder4: Decoder
           PORT MAP(
            code => dis4,
            led => segment4
            );
            
           decoder5: Decoder
           PORT MAP(
            code => dis5,
            led => segment5
            );
            
           decoder6: Decoder
           PORT MAP(
            code => dis6,
            led => segment6
            );
            
           decoder7: Decoder
           PORT MAP(
            code => dis7,
            led => segment7
            );
	       
	       decoder8: Decoder
           PORT MAP(
            code => dis8,
            led => segment8
            );
            
            display: display_refresh
            PORT MAP(
            clk => clk_display, -- 400 Hz
            reset => RESET,
            segment_1 => segment1,
            segment_2 => segment2,
            segment_3 => segment3,
            segment_4 => segment4,
            segment_5 => segment5,
            segment_6 => segment6,
            segment_7 => segment7,
            segment_8 => segment8,
            display_select => display_select,
            display_number => display_number
            );
	       
	   
end Behavioral;
