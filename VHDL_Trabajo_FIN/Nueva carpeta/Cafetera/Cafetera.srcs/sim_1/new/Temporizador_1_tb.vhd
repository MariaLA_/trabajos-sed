library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Temporizador_1_tb is

end Temporizador_1_tb;

architecture Behavioral of Temporizador_1_tb is
component Temporizador_1 is 
port(
 tipo : in std_logic_vector (1 downto 0);
 clk : in std_logic;
 reset : in std_logic;
 ticks : inout integer;
 seg : inout integer;
 activo : out std_logic
);
end component;

signal tipo_tb :  std_logic_vector (1 downto 0):="11";
signal clk_tb, reset_tb, activo_tb : std_logic:='0';
signal seg_tb, ticks_tb : integer:=0;
constant k: time := 20ns;

begin

uut: Temporizador_1 port map(

tipo => tipo_tb,
clk => clk_tb,
reset => reset_tb,
activo => activo_tb

);

clk_tb <= not clk_tb after k;

process 
begin
    wait for k/2;
    reset_tb <= '0';
    tipo_tb <= "00";
    seleccion_tb <= '1';
    wait for k;
    
    wait for k;
   
    wait for k;
    
    wait for 2*k;
    
    wait for k;
    
    wait for 3*k;
    tipo_tb <="11";
    wait for 4*k;
    
    
    
 assert false
 report "[SUCCESS]: simulation finished."
 severity failure;
  
    end process;


end Behavioral;

