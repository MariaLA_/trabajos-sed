library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Temporizador_2_tb is

end Temporizador_2_tb;

architecture Behavioral of Temporizador_2_tb is
component Temporizador_2 is 
port(
 tipo : in std_logic_vector (1 downto 0);
 clk : in std_logic;
 reset : in std_logic;
 seg : inout integer;
 ticks: inout integer;
 activo : out std_logic
);
end component;

signal tipo_tb :  std_logic_vector (1 downto 0):="11";
signal clk_tb, reset_tb, activo_tb : std_logic:='0';
signal seg_tb, ticks_tb : integer:=0;
constant k: time := 20ns;

begin

uut: Temporizador_2 port map(

tipo => tipo_tb,
clk => clk_tb,
reset => reset_tb,
activo => activo_tb,
seg => seg_tb,
ticks => ticks_tb
);

clk_tb <= not clk_tb after k;

process 
begin
    wait for k/2;
    reset_tb <= '0';
    tipo_tb <= "00";
    wait for k;
    
    wait for k;
    tipo_tb <= "01";
    wait for k;
    tipo_tb <= "10";
    wait for 2*k;
    
    wait for k;
   
    wait for 3*k;
    
    wait for 4*k;
    
    
    
 assert false
 report "[SUCCESS]: simulation finished."
 severity failure;
  
    end process;


end Behavioral;
