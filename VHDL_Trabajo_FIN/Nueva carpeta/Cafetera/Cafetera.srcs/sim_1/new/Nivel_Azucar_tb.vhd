library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Nivel_Azucar_tb is
--  Port ( );
end Nivel_Azucar_tb;

architecture Behavioral of Nivel_Azucar_tb is
component Nivel_Azucar is
PORT (
    seleccion : in std_logic:='0'; --Boton para seleccionar la bebida
    cambio : in std_logic:='0'; --Boton para cambiar el tipo de bebida
    clk : in std_logic:='0';
    estado_aux : inout std_logic_vector(1 downto 0):="00";
    --El boton de seleccion tiene prevalencia al de cambio
    estado: out std_logic_vector (1 downto 0)
    );
end component;

signal seleccion_tb, cambio_tb,clk_tb, reset_tb : std_logic:='0';
signal estado_aux_tb, estado_tb : std_logic_vector (1 downto 0):="11";
constant k: time := 20ns;

begin
uut: Nivel_Azucar port map (
    cambio => cambio_tb,
    seleccion => seleccion_tb,
    estado_aux => estado_aux_tb,
    clk => clk_tb,
    estado => estado_tb
    );
    
clk_tb <= not clk_tb after k;

process 
begin
    wait for k/2;
    reset_tb <= '0';
    cambio_tb <= '1';
    wait for k;
    cambio_tb <= '1';
    wait for k;
    cambio_tb <= '1';
    seleccion_tb <= '1';
    wait for k;
    cambio_tb <= '0';
    seleccion_tb <= '0';
    wait for 2*k;
    cambio_tb <= '1';
    wait for k;
    cambio_tb <= '0';
    
    wait for 3*k;
    cambio_tb <= '1';
    seleccion_tb <= '1';
    wait for 4*k;
    
 assert false
 report "[SUCCESS]: simulation finished."
 severity failure;
  
    end process;
end Behavioral;
